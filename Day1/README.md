# DAY 1 - Gitlab orientation and laptop setup for practicals

### FOCUS and LEARNING GOALS
> The aim for this session is for you to 1) become familar with gitlab and how to navigate the practicals, 2) 
> download and install the required programs, and 3) test the scripts used in the practicals.

Doing the practical on your computer is not mandatory as we will have live walk-thoughts where you will be able to see step-by-step what is done. However it can be very useful if you will be doing any of these types of analyses during your Masters, PhD or research work.

### Required downloads   

R version 4.0.0 https://cran.uib.no/   
RStudio https://www.rstudio.com/products/rstudio/download/   

If you are using windows, please install bash to run the suggested commands in the terminal:
https://itsfoss.com/install-bash-on-windows/   

Download PLINK: (PLINK 1.9)[https://www.cog-genomics.org/plink/]

### Specific installations and tests for practicals

Practical 2 - Setup for PLINK QC: https://gitlab.com/huntgenes/SMED8020/-/tree/master/Day2    

Libraries needed for R   
ggplot2   
Scales   

In an R session you can write:   
`install.packages("ggplot2", "scales")`

Practical 5 - Mendelian randomization: https://gitlab.com/huntgenes/SMED8020/-/tree/master/Day5   

Test your setup   
-Download the files for practical 5 from gitlab: https://gitlab.com/huntgenes/SMED8020/-/tree/master/Day5   
-Open Rstudio or R   
-Open  Mendelian_Randomization_Practical_1_-_Single_Sample_-_Rscript.R   
-Edit input directories   
-Run   
-Open  Mendelian_Randomization_Practical_2_-_MR_base_-_Rscript.R   
-Edit input directories   
-Run   
