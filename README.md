# SMED8020 Practicals

[Day 1 - Downloads and setup](Day1)

[Day 2 - QC and data preparation](Day2)

[Day 3 - GWAS and visualising results](Day3)

[Day 4 - GWAS meta-analysis](Day4)

[Day 5 - Mendelian randomization](Day5)
